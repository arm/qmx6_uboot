/*
 *
 * Congatec conga-UMX6 board configuration file.
 *
 * Copyright (C) 2010-2011 Freescale Semiconductor, Inc.
 * Based on Freescale i.MX6Q Sabre Lite board configuration file.
 * Copyright (C) 2015, congatec AG
 * Michael Schanz <michael.schanz@congatec.com>
 *
 * based on the work from Leo Sartre, <lsartre@adeneo-embedded.com>
 *
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __CONFIG_CGTUMX6_H
#define __CONFIG_CGTUMX6_H

#include "mx6_common.h"

#define CONFIG_MACH_TYPE	4122

#ifdef CONFIG_SPL
#define CONFIG_SPL_LIBCOMMON_SUPPORT
#define CONFIG_SPL_MMC_SUPPORT
#define CONFIG_SPL_SPI_SUPPORT
#define CONFIG_SPL_SPI_FLASH_SUPPORT
#define CONFIG_SYS_SPI_U_BOOT_OFFS	(64 * 1024)
#define CONFIG_SPL_SPI_LOAD
#include "imx6_spl.h"
#endif

/* Enable (1) / disable (0) falcon mode  */
#if 0
#define CONFIG_CGT_FALCON_MODE
#endif

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN		(10 * 1024 * 1024)

#define CONFIG_BOARD_EARLY_INIT_F
#define CONFIG_BOARD_LATE_INIT
#define CONFIG_MISC_INIT_R

#define CONFIG_MXC_UART
#define CONFIG_MXC_UART_BASE	       UART2_BASE

/* MMC Configs */
#define CONFIG_SYS_FSL_ESDHC_ADDR      0

/* SPI NOR */
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO
#define CONFIG_SPI_FLASH_SST
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_SPI_FLASH_ATMEL
#define CONFIG_SPI_FLASH_MACRONIX
#define CONFIG_SPI_FLASH_SPANSION
#define CONFIG_MXC_SPI
#define CONFIG_SF_DEFAULT_BUS		0
#define CONFIG_SF_DEFAULT_SPEED		20000000
#define CONFIG_SF_DEFAULT_MODE		(SPI_MODE_0)

/* Miscellaneous commands */
#define CONFIG_CMD_BMODE

/* Thermal support */
#define CONFIG_IMX_THERMAL

/* I2C Configs */
#define CONFIG_CMD_I2C
#define CONFIG_SYS_I2C
#define CONFIG_SYS_I2C_MXC
#define CONFIG_SYS_I2C_MXC_I2C1		/* enable I2C bus 1 */
#define CONFIG_SYS_I2C_MXC_I2C2		/* enable I2C bus 2 */
#define CONFIG_SYS_I2C_MXC_I2C2		/* enable I2C bus 2 */
#define CONFIG_SYS_I2C_SPEED		  100000

/* PMIC */
#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_PFUZE100
#define CONFIG_POWER_PFUZE100_I2C_ADDR	0x08

/* USB Configs */
#define CONFIG_CMD_USB
#define CONFIG_CMD_FAT
#define CONFIG_USB_EHCI
#define CONFIG_USB_EHCI_MX6
#define CONFIG_USB_STORAGE
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET
#define CONFIG_USB_HOST_ETHER
#define CONFIG_USB_ETHER_ASIX
#define CONFIG_MXC_USB_PORTSC	(PORT_PTS_UTMI | PORT_PTS_PTW)
#define CONFIG_MXC_USB_FLAGS	0
#define CONFIG_USB_MAX_CONTROLLER_COUNT 2 /* Enabled USB controller number */
#define CONFIG_USB_KEYBOARD
#define CONFIG_SYS_USB_EVENT_POLL_VIA_CONTROL_EP

#define CONFIG_CI_UDC
#define CONFIG_USBD_HS
#define CONFIG_USB_GADGET_DUALSPEED

#define CONFIG_USB_GADGET
#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_FUNCTION_MASS_STORAGE
#define CONFIG_USB_GADGET_DOWNLOAD
#define CONFIG_USB_GADGET_VBUS_DRAW	2

#define CONFIG_G_DNL_VENDOR_NUM		0x0525
#define CONFIG_G_DNL_PRODUCT_NUM	0xa4a5
#define CONFIG_G_DNL_MANUFACTURER	"Congatec"

/* USB Device Firmware Update support */
#define CONFIG_CMD_DFU
#define CONFIG_USB_FUNCTION_DFU
#define CONFIG_DFU_MMC
#define CONFIG_DFU_SF

#define CONFIG_USB_FUNCTION_FASTBOOT
#define CONFIG_CMD_FASTBOOT
#define CONFIG_ANDROID_BOOT_IMAGE
#define CONFIG_FASTBOOT_BUF_ADDR   CONFIG_SYS_LOAD_ADDR
#define CONFIG_FASTBOOT_BUF_SIZE   0x07000000

/* Framebuffer */
#define CONFIG_VIDEO
#define CONFIG_VIDEO_IPUV3
#define CONFIG_CFB_CONSOLE
#define CONFIG_VGA_AS_SINGLE_DEVICE
#define CONFIG_SYS_CONSOLE_IS_IN_ENV
#define CONFIG_SYS_CONSOLE_OVERWRITE_ROUTINE
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_SPLASH_SCREEN
#define CONFIG_SPLASH_SCREEN_ALIGN
#define CONFIG_BMP_16BPP
#define CONFIG_VIDEO_LOGO
#define CONFIG_VIDEO_BMP_LOGO
#ifdef CONFIG_MX6DL
#define CONFIG_IPUV3_CLK 198000000
#else
#define CONFIG_IPUV3_CLK 264000000
#endif
#define CONFIG_IMX_HDMI

/* SATA */
#define CONFIG_CMD_SATA
#define CONFIG_DWC_AHSATA
#define CONFIG_SYS_SATA_MAX_DEVICE	1
#define CONFIG_DWC_AHSATA_PORT_ID	0
#define CONFIG_DWC_AHSATA_BASE_ADDR	SATA_ARB_BASE_ADDR
#define CONFIG_LBA48
#define CONFIG_LIBATA

/* Ethernet */
#define CONFIG_CMD_PING
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_MII
#define CONFIG_FEC_MXC
#define CONFIG_MII
#define IMX_FEC_BASE			ENET_BASE_ADDR
#define CONFIG_FEC_XCV_TYPE		RGMII
#define CONFIG_ETHPRIME			"FEC"
#define CONFIG_FEC_MXC_PHYADDR		6
#define CONFIG_PHYLIB
#define CONFIG_PHY_ATHEROS

/* Command definition */

#define CONFIG_MXC_UART_BASE	UART2_BASE
#define CONFIG_CONSOLE_DEV	"ttymxc1"
#define CONFIG_MMCROOT		"/dev/mmcblk2p1"
#define CONFIG_SYS_MMC_ENV_DEV		2

#define CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG

#ifndef CONFIG_CGT_MFG
#define CONFIG_EXTRA_ENV_SETTINGS \
	"script=boot.scr\0" \
	"image=uImage\0" \
	"fdtfile=undefined\0" \
	"fdt_addr_r=0x18000000\0" \
	"boot_fdt=try\0" \
	"ip_dyn=yes\0" \
	"console=" CONFIG_CONSOLE_DEV "\0" \
	"dfuspi=dfu 0 sf 0:0:10000000:0\0" \
	"dfu_alt_info_spl=spl raw 0x400\0" \
	"dfu_alt_info_img=u-boot raw 0x10000\0" \
	"dfu_alt_info=spl raw 0x400\0" \
	"bootm_size=0x10000000\0" \
	"mmcdev=" __stringify(CONFIG_SYS_MMC_ENV_DEV) "\0" \
	"mmcpart=1\0" \
	"mmcroot=" CONFIG_MMCROOT " rootwait rw\0" \
	"update_sd_firmware=" \
		"if test ${ip_dyn} = yes; then " \
			"setenv get_cmd dhcp; " \
		"else " \
			"setenv get_cmd tftp; " \
		"fi; " \
		"if mmc dev ${mmcdev}; then "	\
			"if ${get_cmd} ${update_sd_firmware_filename}; then " \
				"setexpr fw_sz ${filesize} / 0x200; " \
				"setexpr fw_sz ${fw_sz} + 1; "	\
				"mmc write ${loadaddr} 0x2 ${fw_sz}; " \
			"fi; "	\
		"fi\0" \
	"mmcargs=setenv bootargs console=${console},${baudrate} " \
		"video=mxcfb0:dev=${vid_dev0} " \
		"video=mxcfb1:dev=${vid_dev1} " \
		"root=${mmcroot}\0" \
	"loadbootscript=" \
		"ext2load mmc ${mmcdev}:${mmcpart} ${loadaddr} ${script};\0" \
	"bootscript=echo Running bootscript from mmc ...; " \
		"source\0" \
	"loadimage=ext2load mmc ${mmcdev}:${mmcpart} ${loadaddr} boot/${image}\0" \
	"loadfdt=ext2load mmc ${mmcdev}:${mmcpart} ${fdt_addr_r} boot/${fdtfile}\0" \
	"mmcboot=echo Booting from mmc ...; " \
		"run mmcargs; " \
		"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
			"if run loadfdt; then " \
				"bootm ${loadaddr} - ${fdt_addr_r}; " \
			"else " \
				"if test ${boot_fdt} = try; then " \
					"bootm; " \
				"else " \
					"echo WARN: Cannot load the DT; " \
				"fi; " \
			"fi; " \
		"else " \
			"bootm; " \
		"fi;\0" \
	"findfdt="\
		"if test $board_rev = MX6Q ; then " \
			"setenv fdtfile imx6q-umx6.dtb; fi; " \
		"if test $board_rev = MX6DL ; then " \
			"setenv fdtfile imx6dl-umx6.dtb; fi; " \
		"if test $fdtfile = undefined; then " \
			"echo WARNING: Could not determine dtb to use; fi; \0" \
	"netargs=setenv bootargs console=${console},${baudrate} " \
		"root=/dev/nfs " \
		"ip=dhcp nfsroot=${serverip}:${nfsroot},v3,tcp\0" \
	"netboot=echo Booting from net ...; " \
		"run netargs; " \
		"if test ${ip_dyn} = yes; then " \
			"setenv get_cmd dhcp; " \
		"else " \
			"setenv get_cmd tftp; " \
		"fi; " \
		"${get_cmd} ${image}; " \
		"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
			"if ${get_cmd} ${fdt_addr_r} ${fdtfile}; then " \
				"bootm ${loadaddr} - ${fdt_addr_r}; " \
			"else " \
				"if test ${boot_fdt} = try; then " \
					"bootm; " \
				"else " \
					"echo WARN: Cannot load the DT; " \
				"fi; " \
			"fi; " \
		"else " \
			"bootm; " \
		"fi;\0" \
	"spilock=sf probe && sf protect lock 0x3f0000 0x10000;\0" \
	"vid_dev0=hdmi,1920x1080M@60,if=RGB24\0" \
	"vid_dev1=ldb,LDB-XGA,if=RGB666\0" \
	"bootcmd_android=run findfdt ; run mmcargs_android ; mmc dev ${mmcdev} ; " \
		"ext2load mmc ${mmcdev}:1 10800000 /uImage && ext2load mmc ${mmcdev}:1 " \
		"12800000 /uramdisk.img && ext2load mmc ${mmcdev}:1 0x12000000 /${fdtfile} ; " \
		"bootm 10800000 12800000 12000000 \0" \
	"mmcargs_android=setenv bootargs console=${console},${baudrate} video=mxcfb0:dev=${vid_dev0} " \
		"video=mxcfb1:dev=${vid_dev1} init=/init vmalloc=400M  androidboot.console=ttymxc1 " \
		"consoleblank=0 androidboot.hardware=congatec androidboot.bootdev=mmcblk${mmcdev} ldo_active=0 \0" \

#define CONFIG_BOOTCOMMAND \
	"run spilock;"	    \
	"run findfdt; "	\
	"mmc dev ${mmcdev};" \
	"if mmc rescan; then " \
		"if run loadbootscript; then " \
		"run bootscript; " \
		"else " \
			"if run loadimage; then " \
				"run mmcboot; " \
			"else run netboot; " \
			"fi; " \
		"fi; " \
	"else run netboot; fi"

#else

#define CONFIG_BOOTCOMMAND	"bootm 0x11000000 0x13000000 0x18000000;"

#define CONFIG_BOOTARGS		"console=ttymxc1,115200 rdinit=/linuxrc "	\
				"g_mass_storage.stall=0 g_mass_storage.removable=1 " \
				"g_mass_storage.idVendor=0x066F " \
				"g_mass_storage.idProduct=0x37FF " \
				"g_mass_storage.iSerialNumber="" enable_wait_mode=off"


#define CONFIG_EXTRA_ENV_SETTINGS \
                "bootm_size=0x10000000\0" \
                "bootdelay=1\0"

#endif

#define CONFIG_SYS_MEMTEST_START       0x10000000
#define CONFIG_SYS_MEMTEST_END	       0x10010000
#define CONFIG_SYS_MEMTEST_SCRATCH     0x10800000

/* Physical Memory Map */
#define CONFIG_NR_DRAM_BANKS	       1
#define PHYS_SDRAM		       MMDC0_ARB_BASE_ADDR
#define PHYS_SDRAM_SIZE			       (1u * 1024 * 1024 * 1024)

#define CONFIG_SYS_SDRAM_BASE	       PHYS_SDRAM
#define CONFIG_SYS_INIT_RAM_ADDR       IRAM_BASE_ADDR
#define CONFIG_SYS_INIT_RAM_SIZE       IRAM_SIZE

#define CONFIG_SYS_INIT_SP_OFFSET \
	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

/* Environment organization */
#if defined (CONFIG_ENV_IS_IN_MMC)
#define CONFIG_ENV_OFFSET		(6 * 64 * 1024)
#define CONFIG_SYS_MMC_ENV_DEV		0
#endif

#define CONFIG_ENV_SIZE			(8 * 1024)

#ifndef CONFIG_CGT_MFG
#define CONFIG_ENV_IS_IN_SPI_FLASH
#else
#define CONFIG_ENV_IS_NOWHERE
#endif

#if defined(CONFIG_ENV_IS_IN_SPI_FLASH)
#define CONFIG_ENV_OFFSET		(768 * 1024)
#define CONFIG_ENV_SECT_SIZE		(64 * 1024)
#define CONFIG_ENV_SPI_BUS		CONFIG_SF_DEFAULT_BUS
#define CONFIG_ENV_SPI_CS		CONFIG_SF_DEFAULT_CS
#define CONFIG_ENV_SPI_MODE		CONFIG_SF_DEFAULT_MODE
#define CONFIG_ENV_SPI_MAX_HZ		CONFIG_SF_DEFAULT_SPEED
#endif

/*
 * Falcon mode
 */
#ifdef CONFIG_CGT_FALCON_MODE

#ifndef CONFIG_SPL
#error "CONFIG_CGT_FALCON_MODE depends on CONFIG_SPL"
#endif

/* Enable/disable boot mode button */
#ifdef CONFIG_CGT_FALCON_MODE_IMG_SEL_GPIO
/* adapt BOOT_MODE_BTN to your needs */
#define BOOT_MODE_BTN IMX_GPIO_NR(4, 6) /* KEY_LID_BTN */
#define CGT_BOOT_MODE_BTN_ENABLED
#endif

/* Enable/disable environment boot mode selection */
#ifdef CONFIG_CGT_FALCON_MODE_IMG_SEL_ENV
#define CONFIG_SPL_ENV_SUPPORT
#endif

/* Enable/disable execution of kernel uImage by SPL */
#ifdef CONFIG_CGT_FALCON_MODE_OS_BOOT
#define CONFIG_SPL_OS_BOOT
#endif

/*
 * Selecting MMC device (dev kernel/uboot.img is stored at...)
 * usually selecty by configuration (Kconfig)...
 */
/* #define CONFIG_CGT_UMX6_FALCON_MODE_BD_EMMC */
/* #define CONFIG_CGT_UMX6_FALCON_MODE_BD_SD */

/* CONFIG_SPL_OS_BOOT (falcon mode) essentials */
/* Enabling <spl export> command */
#define CONFIG_CMD_SPL
/* Address in RAM where the paramters must be copied to by SPL. */
#define CONFIG_SYS_SPL_ARGS_ADDR       0x18000000
/* Size of the parameters area to be copied */
#define CONFIG_CMD_SPL_WRITE_SIZE      (128 * SZ_1K)

/* Storing kernel image at MMC device (uSD, EMMC or SD) */
/* Argument file has to be stored at sector 0x800. Sector size = 512 Bytes
 *  => 0x800: 0x800 * 512 Byte = 1048576 Bytes / 1024 / 1024 = 1MB offset
 */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTOR  0x800
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTORS (CONFIG_CMD_SPL_WRITE_SIZE / 512)
/* Kernel image file has to be stored at sector 0x1000. Sector size = 512 Bytes
 *  => 0x1000: 0x1000 * 512 Byte = 2097152 Bytes / 1024 / 1024 = 2MB offset
 *  => First sector of first MMC partition starting at sector 24576 or 32768
 */
#define CONFIG_SYS_MMCSD_RAW_MODE_KERNEL_SECTOR 0x1000

/*  SPI dummy entries:
 *  falcon mode implementation assumes that SPL and kernel image are stored at
 *  the same storage device.
 *  At congatec designs, SPL and uboot.img are usually stored at SPI-flash.
 *  The SPI-flash's size is limited, so the uImage can't be stored at the
 *  SPI-flash in most cases.
 *  Thence, the congatec implementation recommends following setup:
 *  - SPL stored at SPI-flash
 *  - uImage stored at MMC device (uSD, EMMC or SD)
 *  Avoiding compiler errors demands the definition of the following SPI related
 *  constants. Due to storage of the uImage at MMC device, they are NOT used.
 */
#define CONFIG_SYS_SPI_ARGS_OFFS CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTOR
#define CONFIG_SYS_SPI_ARGS_SIZE (CONFIG_SYS_SPI_KERNEL_OFFS - CONFIG_SYS_SPI_ARGS_OFFS)
#define CONFIG_SYS_SPI_KERNEL_OFFS CONFIG_SYS_MMCSD_RAW_MODE_KERNEL_SECTOR

#if defined(CONFIG_CGT_UMX6_FALCON_MODE_BD_EMMC)
#define CGT_FALCON_MODE_EMMC
#elif defined(CONFIG_CGT_UMX6_FALCON_MODE_BD_SD)
#define CGT_FALCON_MODE_SD
#else
#error "CONFIG_CGT_FALCON_MODE - device holding boot image is not defined"
#endif

#endif /* CONFIG_CGT_FALCON_MODE */

#endif /* __CONFIG_CGTUMX6_H */
