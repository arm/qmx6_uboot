if TARGET_CGTQMX6EVAL

config SYS_BOARD
	default "cgtqmx6eval"

config SYS_VENDOR
	default "congatec"

config SYS_CONFIG_NAME
	default "cgtqmx6eval"


menu "congatec misc"

config CGT_MFG
	bool "Build manufacturing u-Boot (mfg environment)"
	depends on !SPL

menu "falcon mode"
	depends on SPL

config CGT_FALCON_MODE
	bool "Enable falcon mode"
	depends on SPL
	help
	 This option activates falcon mode at congatec qmx6/umx6 designs.
	 Falcon mode is mainly used to shorten boot time (quickboot).
	 There are the following options:
	   (a) Direct execution of kernel image by SPL (quickboot).
	       In order to load a kernel image directly,
	       <CONFIG_CGT_FALCON_MODE_OS_BOOT> has to be set.
	   (b) Storing/loading uboot.img from MMC device instead of SPI-flash.
	       Please note, the environment has still to be stored at SPI-flash.
	       In order to load uboot.img from MMC device,
	       <CONFIG_CGT_FALCON_MODE_OS_BOOT> has to be unset.

	 General information:
	 Falcon mode extends/changes boot order from SPI (1) to MMC
	 (1), SPI (2).
	 If <CGT_FALCON_MODE_OS_BOOT> is set, each device is searched
	 for kernel image first, if there is no kernel image, it is
	 searched for uboot.img afterwards.


choice
	prompt "Device holding kernel/uboot.img"
	depends on CGT_FALCON_MODE
	default CGT_QMX6_FALCON_MODE_BD_USD
	help
	 In falcon mode, congatec designs load the boot image from MMC
	 at first.
	 Further information dependent on boot mode:
	  (a) Quickboot
	      Due to limited size of SPI-flash, the kernel image has to be stored
	      at MMC device instead of SPI-flash. Select MMC device of choice.
	  (b) Storing uboot.img at MMC device
	      In order to store/load uboot.img at/from MMC device, specify MMC
	      device of choice.

config CGT_QMX6_FALCON_MODE_BD_USD
	bool "Load image from uSD card"

config CGT_QMX6_FALCON_MODE_BD_EMMC
	bool "Load image from EMMC"

config CGT_QMX6_FALCON_MODE_BD_SD
	bool "Load image from SD card"

endchoice

config CGT_FALCON_MODE_OS_BOOT
	bool "Enable load/execution of the kernel image via SPL"
	depends on CGT_FALCON_MODE
	help
	 This option enables direct load/execution of the kernel image
	 via SPL (quickboot).
	 If unset, SPL trys to load uboot.img instead.

config CGT_FALCON_MODE_IMG_SEL_GPIO
	bool "Enable boot selection via GPIO"
	depends on CGT_FALCON_MODE && CGT_FALCON_MODE_OS_BOOT
	help
	 Switching boot image (kernel/uboot.img) via GPIO.
	 Default: LID button, customizable via <BOOT_MODE_BTN>.

config CGT_FALCON_MODE_IMG_SEL_ENV
	bool "Enable boot selection via environment settings"
	depends on CGT_FALCON_MODE && CGT_FALCON_MODE_OS_BOOT
	help
	 If enabled, direct execution of the kernel image depends on
	 the environment variable <boot_os>.
	 In order to start the kernel image directly, <boot_os> has to
	 be set to 1.
	 Default: <boot_os> is NOT defined/set.

	 ATTENTION: if <boot_os> is set to 1, SPL always loads the kernel
	 image directly.
	 There are two ways to get back to the uboot commandline:
	 - ejecting MMC:
	   if there is no valid kernal image at the specified MMC device
	   or there is no mmc device, uboot.img is loaded as a fallback.
	 - GPIO override:
	   if <CGT_FALCON_MODE_IMG_SEL_GPIO> is also enabled, loading
	   uboot.img can be forced via GPIO.

endmenu

endmenu

endif
